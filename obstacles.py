import random


class Obstacle:
    def __init__(self):
        self.x_1 = random.randrange(1, 84)
        self.y_1 = 1
        self.x_2 = self.x_1 + random.randrange(3, 6)
        self.y_2 = random.randrange(3, 7)

    def move(self, v):
        self.y_1 = self.y_1 + v
        self.y_2 = self.y_2 + v

    def delete_obstacle(self):
        if self.y_1 > 84:
            del self
