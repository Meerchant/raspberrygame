class Player:
    def __init__(self, start_x, start_y, width, height):
        self.x_1 = start_x
        self.y_1 = start_y
        self.x_2 = start_x + width
        self.y_2 = start_y + height
        self.front_wing_x_1 = self.x_1 - 3
        self.front_wing_y_1 = self.y_1 + 2
        self.front_wing_x_2 = self.x_2 + 3
        self.front_wing_y_2 = self.y_1 + 3
        self.back_wing_x_1 = self.x_1 - 1
        self.back_wing_y_1 = self.y_2
        self.back_wing_x_2 = self.x_2 + 1
        self.back_wing_y_2 = self.y_2

    def check_front_wing_collision(self, obstacle):
        if self.front_wing_x_1 <= obstacle.x_1 <= self.front_wing_x_2 or self.front_wing_x_1 <= obstacle.x_2 <= self.front_wing_x_2:
            if self.front_wing_y_1 <= obstacle.y_1 <= self.front_wing_y_2 or self.front_wing_y_1 <= obstacle.y_2 <= self.front_wing_y_2:
                return True
            else:
                return False
        else:
            return False

    def check_back_wing_collision(self, obstacle):
        if self.back_wing_x_1 <= obstacle.x_1 <= self.back_wing_x_2 or self.back_wing_x_1 <= obstacle.x_2 <= self.back_wing_x_2:
            if self.back_wing_y_1 <= obstacle.y_1 <= self.back_wing_y_2 or self.back_wing_y_1 <= obstacle.y_2 <= self.back_wing_y_2:
                return True
            else:
                return False
        else:
            return False

    def check_cabin_wing_collision(self, obstacle):
        if self.x_1 <= obstacle.x_1 <= self.x_2 or self.x_1 <= obstacle.x_2 <= self.x_2:
            if self.y_1 <= obstacle.y_1 <= self.y_2 or self.y_1 <= obstacle.y_2 <= self.y_2:
                return True
            else:
                return False
        else:
            return False

    def check_wall_collision(self):
        if self.front_wing_x_1 < 1 or self.front_wing_x_2 > 83:
            return True
        else:
            return False

    def collision_check(self, obstacles_tab):

        for obstacle in obstacles_tab:
            if self.check_front_wing_collision(obstacle) or self.check_back_wing_collision(
                    obstacle) or self.check_cabin_wing_collision(obstacle) or self.check_wall_collision():
                return True

        return False

    def move_right(self, v):
        self.x_1 = self.x_1 + v
        self.x_2 = self.x_2 + v
        self.front_wing_x_1 = self.front_wing_x_1 + v
        self.front_wing_x_2 = self.front_wing_x_2 + v
        self.back_wing_x_1 = self.back_wing_x_1 + v
        self.back_wing_x_2 = self.back_wing_x_2 + v

    def move_left(self, v):
        self.x_1 = self.x_1 - v
        self.x_2 = self.x_2 - v
        self.front_wing_x_1 = self.front_wing_x_1 - v
        self.front_wing_x_2 = self.front_wing_x_2 - v
        self.back_wing_x_1 = self.back_wing_x_1 - v
        self.back_wing_x_2 = self.back_wing_x_2 - v
