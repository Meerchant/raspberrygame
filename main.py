import adafruit_pcd8544
import time
import board
import busio
import digitalio
from PIL import Image, ImageDraw
from player import Player
from obstacles import Obstacle
from canon import Canon
import random


def initialize_lcd():
    spi = busio.SPI(board.SCK, MOSI=board.MOSI)
    dc = digitalio.DigitalInOut(board.D6)  # data/command
    cs = digitalio.DigitalInOut(board.D8)  # Chip select
    reset = digitalio.DigitalInOut(board.D5)  # reset
    back_light = digitalio.DigitalInOut(board.D13)  # back_light
    back_light.switch_to_output()
    back_light.value = True
    return adafruit_pcd8544.PCD8544(spi, dc, cs, reset)


# LCD set up
display = initialize_lcd()
display.bias = 4
display.contrast = 35
# Controls setup
switch_right = digitalio.DigitalInOut(board.D14)
switch_right.switch_to_input(pull=digitalio.Pull.UP)
switch_left = digitalio.DigitalInOut(board.D15)
switch_left.switch_to_input(pull=digitalio.Pull.UP)


def update_screen(player, obstacles_tab, points, bullet_obj, rocket_count):
    image = Image.new('1', (display.width, display.height))
    draw = ImageDraw.Draw(image)
    draw.rectangle((0, 0, display.width, display.height), outline=255, fill=255)
    draw.rectangle((1, 0, display.width - 2, display.height),
                   outline=0, fill=0)
    for obstacle in obstacles_tab:
        draw.rectangle((obstacle.x_1, obstacle.y_1, obstacle.x_2, obstacle.y_2), outline=255, fill=255)
    draw.rectangle((player.x_1, player.y_1, player.x_2, player.y_2), outline=255, fill=255)
    draw.rectangle((player.front_wing_x_1, player.front_wing_y_1, player.front_wing_x_2, player.front_wing_y_2),
                   outline=255, fill=255)
    draw.rectangle((player.back_wing_x_1, player.back_wing_y_1, player.back_wing_x_2, player.back_wing_y_2),
                   outline=255, fill=255)
    if bullet_obj:
        draw.rectangle((bullet_obj.x_1, bullet_obj.y_1, bullet_obj.x_2, bullet_obj.y_2), outline=255, fill=255)
    if player_1.x_1 > 5:
        draw.text((2, 38), str(rocket_count), fill=255)
    if player_1.x_2 < 65:
        draw.text((65, 38), str(points), fill=255)
    display.image(image)
    display.show()
    
def new_record(points):
    try:
        file = open("Points.txt", 'r')     
        record = file.read()
        file.close()
    except IOError:
        file = open("Points.txt", 'w')
        file.write(str(points))
        file.close()
        return True
    if points > int(record):
        file = open("Points.txt", 'w')
        file.write(str(points))
        file.close()
        is_new_record = True
    else:
        is_new_record = False
    return is_new_record


def game_over_screen(points):
    image = Image.new('1', (display.width, display.height))
    draw = ImageDraw.Draw(image)
    draw.rectangle((0, 0, display.width, display.height), outline=255, fill=255)
    draw.rectangle((3, 3, display.width - 3, display.height - 3), outline=0, fill=0)
    text_1 = "GAME OVER"
    text_2 = str(points) + " points"
    text_3 = "Play again ->"
    recorded = new_record(points)
    if recorded==True:
        text_4 = "New record!"
    draw.text((15, 5), text_1, fill=255)
    draw.text((15, 15), text_2, fill=255)
    draw.text((4, 30), text_3, fill=255)
    display.image(image)
    display.show()
    while True:
        if not switch_right.value:
            break
        time.sleep(0.1)


def crash_screen(player, obstacles_tab, points):
    for r in range(2, 6):
        image = Image.new('1', (display.width, display.height))
        draw = ImageDraw.Draw(image)
        draw.rectangle((0, 0, display.width, display.height), outline=255, fill=255)
        draw.rectangle((1, 0, display.width - 2, display.height),
                       outline=0, fill=0)
        for obstacle in obstacles_tab:
            draw.rectangle((obstacle.x_1, obstacle.y_1, obstacle.x_2, obstacle.y_2), outline=255, fill=255)
        draw.rectangle((player.x_1, player.y_1, player.x_2, player.y_2), outline=255, fill=255)
        draw.rectangle((player.front_wing_x_1, player.front_wing_y_1, player.front_wing_x_2, player.front_wing_y_2),
                       outline=255, fill=255)
        draw.rectangle((player.back_wing_x_1, player.back_wing_y_1, player.back_wing_x_2, player.back_wing_y_2),
                       outline=255, fill=255)
        draw.text((65, 38), str(points), fill=255)
        for k in range(5):
            draw.line((player.x_1 + random.randrange(-4, 4), player.y_1 + random.randrange(-4, 4),
                       player.x_2 + random.randrange(-4, 4), player.y_2 + random.randrange(-4, 4)), fill=255, width=1)
        display.image(image)
        display.show()
        time.sleep(0.5)


def generate_and_move_obstacles(actual_counter, hard_l, obstacles_tab):
    if actual_counter % hard_l == 0:
        obstacle_tab.append(Obstacle())
        for obstacle in obstacles_tab:
            obstacle.move(2)
            obstacle.delete_obstacle()


def controls_interface():
    if not switch_left.value:
        player_1.move_left(1)
    if not switch_right.value:
        player_1.move_right(1)


def launch_rocket(rocket_count):
    if not switch_right.value:
        time.sleep(0.005)
        if not switch_left.value and rocket_count > 0:
            return True
        else:
            return False
    else:
        return False


def hard_level_control(actual_counter):
    if actual_counter > 300:
        return 5
    if actual_counter > 500:
        return 4
    if actual_counter > 700:
        return 3
    else:
        return 7


while True:
    player_1 = Player(display.width / 2 - 1, display.height - 17, 1, 8)
    obstacle_tab = []
    counter = 1
    hard_level = 7
    rocket_counter = 3
    bullet = None
    points = 0
    while True:
        controls_interface()
        if launch_rocket(rocket_counter) and not bullet:
            bullet = Canon(player_1.x_1, player_1.y_1)
            rocket_counter = rocket_counter - 1
        update_screen(player_1, obstacle_tab, points, bullet, rocket_counter)
        if player_1.collision_check(obstacle_tab):
            crash_screen(player_1, obstacle_tab, points)
            break
        if bullet:
            if bullet.check_collision(obstacle_tab):
                obstacle_tab.remove(bullet.get_target_obstacle(obstacle_tab))
                bullet = None
        generate_and_move_obstacles(counter, hard_level_control(points), obstacle_tab)
        if bullet:
            bullet.move_bullet(1)
        if counter % 3 == 0:
            points = points + 1
        counter = counter + 1

    game_over_screen(points)
