class Canon:
    def __init__(self, x_1, y_1):
        self.x_1 = x_1
        self.y_1 = y_1
        self.x_2 = x_1 + 1
        self.y_2 = y_1 + 1

    def move_bullet(self, v):
        self.y_1 = self.y_1 - v
        self.y_2 = self.y_2 - v

    def check_collision(self, obstacles_tab):
        for obstacle in obstacles_tab:
            if obstacle.x_1 <= self.x_1 <= obstacle.x_2 or obstacle.x_1 <= self.x_2 <= obstacle.x_2:
                if obstacle.y_1 <= self.y_1 <= obstacle.y_2 or obstacle.y_1 <= self.y_2 <= obstacle.y_2:
                    return True

        else:
            return False

    def get_target_obstacle(self, obstacles_tab):
        for obstacle in obstacles_tab:
            if obstacle.x_1 <= self.x_1 <= obstacle.x_2 or obstacle.x_1 <= self.x_2 <= obstacle.x_2:
                if obstacle.y_1 <= self.y_1 <= obstacle.y_2 or obstacle.y_1 <= self.y_2 <= obstacle.y_2:
                    return obstacle
